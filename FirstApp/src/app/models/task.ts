export class Task {
    id: number;
    name: string;
    description: string;
    state: boolean;
}
