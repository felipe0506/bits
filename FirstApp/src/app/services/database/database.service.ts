import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  private database: SQLiteObject;

  constructor(private sqlite: SQLite) {
    console.log('DB READY');
    this.sqlite.create({
      name: 'bits',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.database = db;
        this.createTables();
      })  
      .catch(e => console.log(e));
  }

  async createTables() {
    let sql = 
    `CREATE TABLE IF NOT EXISTS project (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT,
      description TEXT,
      state TEXT
    );`;
    return this.database.executeSql(sql, [])
    .then(res => console.log('Executed SQL'))
    .catch(e => console.log(e));
  }
  
}
