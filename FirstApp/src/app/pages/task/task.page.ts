import { Component, OnInit } from '@angular/core';
// Models
import { Task } from '../../models/task';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {

  tasks: Task[] =[
    {
      id: 1,
      name: 'Task 1',
      description: 'test task 1',
      state: true
    },
    {
      id: 2,
      name: 'Task 2',
      description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      state: true
    },
    {
      id: 3,
      name: 'Task 3',
      description: 'test task 3',
      state: false
    },
  ];

  constructor(public alertController: AlertController) { }

  ngOnInit() {
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Delete project ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
  
  async newProject() {
    const projectToInsert = await this.alertController.create({
      header: 'New Project',
      inputs: [
        {
          name: 'txtName',
          type: 'text',
          placeholder: 'Name'
        },
        {
          name: 'txtDescripcion',
          type: 'text',
          placeholder: 'Description'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Save',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await projectToInsert.present();
  }

  async editProject() {
    const projectToUpdate= await this.alertController.create({
      header: 'Edit Project',
      inputs: [
        {
          name: 'txtName',
          type: 'text',
          placeholder: 'Name'
        },
        {
          name: 'txtDescripcion',
          type: 'text',
          placeholder: 'Description'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Save',
          handler: (  data ) => {
            console.log('Confirm Ok',data);
          }
        }
      ]
    });
    await projectToUpdate.present();
  }

}
